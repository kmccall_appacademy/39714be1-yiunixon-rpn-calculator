class RPNCalculator

  attr_accessor :stack


#
  def initialize
    @stack = []
  end

  def value
    stack.last
  end

  def push(num)
    stack << num
  end

  def raise_error
    raise "calculator is empty"
  end

  def get_operators
    second_operand = stack.pop
    first_operand = stack.pop
    if first_operand && second_operand
      return [first_operand,second_operand]
    else
      raise_error
    end

  end

  def plus
    operators = get_operators
    stack << operators[0] + operators[1]
  end

  def minus
    operators = get_operators
    stack << operators[0] - operators[1]
  end

  def divide
    operators = get_operators
    stack << operators[0].fdiv(operators[1])
  end

  def times
    operators = get_operators
    stack << operators[0] * operators[1]
  end

  def tokens(string)
    string.split.map do |el|
      if el.match(/[+\-*\/]/)
        el.to_sym
      else
        el.to_i
      end
    end
  end

  def evaluate(string)
    tokens(string).each do |el|
      if el.is_a? Integer
        push el
      else
        case el
        when :+
          plus
        when :-
          minus
        when :*
          times
        when :/
          divide
        end
      end
    end
    value
  end

end
